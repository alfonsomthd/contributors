#!/bin/bash

set -e

usermod -u $(stat -c '%u' "$0") www-data

cp /var/www/app/config/parameters.yml.dist /var/www/app/config/parameters.yml
rm -rf /var/www/var/cache/*

readonly PHP_VERSION=7.2
readonly SCRIPT_DIR=$(pwd)/$(dirname "$0")
sed -i.default -e 's/^listen = .*$/listen = 127.0.0.1:9000/g' /etc/php/"$PHP_VERSION"/fpm/pool.d/www.conf
ln -sf "$SCRIPT_DIR"/xdebug.ini /etc/php/"$PHP_VERSION"/mods-available/xdebug.ini

/etc/init.d/php"$PHP_VERSION"-fpm start

ln -sf "$SCRIPT_DIR"/nginx.conf /etc/nginx/nginx.conf
ln -sf "$SCRIPT_DIR"/nginx-api.conf /etc/nginx/conf.d/www.conf
readonly NGINX_CACHE_DIR=/var/cache/nginx
rm -rf "$NGINX_CACHE_DIR" && mkdir -p "$NGINX_CACHE_DIR"

chown -R www-data:www-data /var/www "$NGINX_CACHE_DIR"

exec nginx -g 'daemon off;'
