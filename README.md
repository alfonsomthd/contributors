# REST API

This app returns a list of the Top50/100/150 GitHub contributors by city (sorted by number of repositories).

## Explanation

* **App**: Built with PHP 7 + Symfony 3.

* **Architecture**: REST API following Hexagonal Architecture + Domain Driven Design (DDD).
    Although this tiny app **does not** require DDD approach, DDD is implemented as a Proof of Concept.
    The app architecture (**src** folder) has 4 layers:

    * User Interface (Presentation)
    * Application
    * Domain
    * Infrastructure

* **Authentication**: Json Web Token. Usernames and passwords are stored in MySQL using Doctrine2 ORM.

* **Web Server**: NGINX as FastCGI proxy + PHP-FPM (FastCGI Process Manager).

* **Cache**: NGINX as 1st cache layer and Redis as 2nd cache layer.

## Installation

* Install [Docker Compose](https://docs.docker.com/compose/install/).

* Install project:
```
git clone https://bitbucket.org/alfonsomthd/contributors.git
cd contributors
cp .env.example .env
docker-compose run --rm api bash install.sh
```

## Usage

By default, the app root endpoint is: http://localhost:8090/api/v1.

If you want to change the port number, edit *APP_PUBLISHED_PORT* in *.env* file.

Start project:
```
docker-compose up -d
```

* If you want to stop it:
    ```
    docker-compose down
    ```

Run unit tests:
```
docker-compose exec api vendor/bin/phpunit
```

Run acceptance tests:
```
docker-compose exec api vendor/bin/codecept run
```

The "test" user is already available.

First, authenticate (in production environment, this request has to be made over HTTPS):
```
curl -XPOST -H "Content-Type: application/json" http://localhost:8090/api/v1/login -d '{"username": "test", "password": "test1"}'
```

The endpoint follows this pattern:
```
/contributors?city={city}&amount={amount}
```

List Top50 GitHub London contributors (replace *yourToken* with the token received before):

```
curl -XGET -H "Authorization: Bearer {yourToken}" http://localhost:8090/api/v1/contributors?city=london&amount=50
```
