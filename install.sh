#!/bin/bash

set -e

usermod -u $(stat -c '%u' "$0") www-data

chown -R www-data:www-data var/*

su www-data -s /bin/bash << EOT

php bin/composer install

cp app/config/parameters.yml.dist app/config/parameters.yml

php bin/console doctrine:database:drop --if-exists --force
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force

php bin/console api:load-user-initial-data && echo 'Project installation finished!!!'

EOT
