<?php

class ApiCest
{
    public function getContributors(ApiTester $I)
    {
        $I->sendGET('/');
        $I->seeResponseCodeIs(200);

        $I->sendPOST(
            '/login',
            json_encode(
                [
                    'username' => 'test',
                    'password' => 'test1',
                ]
            )
        );
        $I->seeResponseCodeIs(200);
        $authToken = json_decode($I->grabResponse(), true)['token'];

        $I->haveHttpHeader('Authorization', 'Bearer '.$authToken);
        $city = 'london';
        $amount = 5;
        $I->sendGET(
            '/contributors',
            ['city' => $city, 'amount' => $amount]
        );
        $I->seeResponseCodeIs(200);
        $contributors = json_decode($I->grabResponse());

        $I->assertSame($amount, count($contributors));
    }
}
