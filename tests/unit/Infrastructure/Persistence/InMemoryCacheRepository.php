<?php

namespace ApiDemo\Tests\Unit\Infrastructure\Persistence;

use Symfony\Component\Cache\Simple\NullCache;

class InMemoryCacheRepository extends NullCache
{
    private $data;
    
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    public function get($key, $default = null)
    {
        return array_key_exists($key, $this->data) ? $this->data[$key]: $default;
    }

    public function has($key)
    {
        return array_key_exists($key, $this->data);
    }
}
