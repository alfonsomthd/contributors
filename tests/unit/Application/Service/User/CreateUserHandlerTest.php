<?php

namespace ApiDemo\Tests\Unit\Application\Service\User;

use ApiDemo\Application\DataTransformer\User\ArrayUserDataTransformer;
use ApiDemo\Application\Service\User\CreateUserHandler;
use ApiDemo\Application\Service\User\CreateUserCommand;
use ApiDemo\Infrastructure\Domain\Model\User\InMemoryUserRepository;

class CreateUserHandlerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @test
     */
    public function userIsCreated()
    {
        $request = new CreateUserCommand();
        $request->name = 'test';
        $request->password = 'testpass';

        $handler = new CreateUserHandler(
            new InMemoryUserRepository(),
            new ArrayUserDataTransformer()
        );
        $userData = $handler->handle($request);

        $this->assertSame('test', $userData['name']);
    }
}
