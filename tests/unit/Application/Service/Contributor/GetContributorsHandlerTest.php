<?php

namespace ApiDemo\Tests\Unit\Application\Service\Contributor;

use ApiDemo\Application\DataTransformer\Contributor\ArrayContributorDataTransformer;
use ApiDemo\Application\Service\Contributor\GetContributorsCommand;
use ApiDemo\Application\Service\Contributor\GetContributorsHandler;
use ApiDemo\Domain\Model\Contributor\Contributor;
use ApiDemo\Domain\Model\Contributor\ContributorGateway;
use PHPUnit\Framework\TestCase;

class GetContributorsHandlerTest extends TestCase
{
    /**
     * @test
     */
    public function returnsContributorsOnValidRequestData()
    {
        $contributorGatewayStub = $this->createMock(ContributorGateway::class);
        $contributors = [
            new Contributor(1, 'github', 'contributor1'),
            new Contributor(2, 'github', 'contributor2'),
            new Contributor(3, 'github', 'contributor3'),
        ];
        $contributorGatewayStub->method('listBy')->willReturn($contributors);
        $handler = $this->getHandler($contributorGatewayStub);

        $command = new GetContributorsCommand();
        $command->platform = '';
        $command->city = 'london';
        $command->amount = 3;

        $result = $handler->handle($command);

        $expectedResult = [
            ['id' => 1, 'name' => 'contributor1'],
            ['id' => 2, 'name' => 'contributor2'],
            ['id' => 3, 'name' => 'contributor3'],
        ];

        $this->assertSame($expectedResult, $result);
    }

    /**
     * @test
     */
    public function returnsContributorsOnEmptyPlatform()
    {
        $contributorGatewayStub = $this->createMock(ContributorGateway::class);
        $contributors = [
            new Contributor(3, 'github', 'contributor3'),
        ];
        $contributorGatewayStub->method('listBy')->willReturn($contributors);
        $handler = $this->getHandler($contributorGatewayStub);

        $command = new GetContributorsCommand();
        $command->platform = '';
        $command->city = 'barcelona';
        $command->amount = 3;

        $result = $handler->handle($command);

        $expectedResult = [
            ['id' => 3, 'name' => 'contributor3'],
        ];

        $this->assertSame($expectedResult, $result);
    }

    /**
     * @test
     */
    public function throwsExceptionOnInvalidPlatform()
    {
        $contributorGatewayStub = $this->createMock(ContributorGateway::class);
        $handler = $this->getHandler($contributorGatewayStub);

        $command = new GetContributorsCommand();
        $command->platform = 'nonexistent_platform';

        $this->expectExceptionMessage('Invalid platform: "'.$command->platform.'"');

        $handler->handle($command);
    }

    /**
     * @test
     */
    public function throwsExceptionOnInvalidCity()
    {
        $contributorGatewayStub = $this->createMock(ContributorGateway::class);
        $handler = $this->getHandler($contributorGatewayStub);

        $command = new GetContributorsCommand();
        $command->platform = 'github';
        $command->city = '';

        $this->expectExceptionMessage('Invalid city: "'.$command->city.'"');

        $handler->handle($command);
    }

    private function getHandler(ContributorGateway $contributorGatewayStub)
    {
        return new GetContributorsHandler(
            $contributorGatewayStub,
            new ArrayContributorDataTransformer()
        );
    }
}
