<?php

namespace ApiDemo\Infrastructure\Service;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Psr7\Response;

class HttpClient extends Client
{
    public function __construct(array $config = [])
    {
        $config += ['verify' => false];

        parent::__construct($config);
    }

    public function retrieve(array $uris)
    {
        $promises = [];
        foreach ($uris as $uri) {
            $promises[] = $this->getAsync($uri);
        }

        $responsesData = Promise\settle($promises)->wait();

        $results = [];
        /** @var Response $response */
        foreach ($responsesData as $responseData) {
            $response = $responseData['value'];
            $results[] = json_decode($response->getBody()->getContents());
        }

        return $results;
    }
}
