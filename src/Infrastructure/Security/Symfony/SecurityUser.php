<?php

namespace ApiDemo\Infrastructure\Security\Symfony;

use Symfony\Component\Security\Core\User\UserInterface;

class SecurityUser implements UserInterface
{
    private $username;
    private $password;

    public function __construct($username, $password = null)
    {
        $this->username = $username;
        $this->password = $password;
    }

    public function getRoles()
    {
        return 'admin' === $this->username ? ['ROLE_ADMIN'] : ['ROLE_USER'];
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function eraseCredentials()
    {
    }
}
