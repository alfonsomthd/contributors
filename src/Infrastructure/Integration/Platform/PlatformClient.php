<?php

namespace ApiDemo\Infrastructure\Integration\Platform;

use ApiDemo\Infrastructure\Service\HttpClient;

abstract class PlatformClient
{
    protected $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    abstract public function getContributorsData($city): array;
}
