<?php

namespace ApiDemo\Infrastructure\Integration\Platform;

class GitHubClient extends PlatformClient
{
    private $rootEndpoint = 'https://api.github.com';

    public function getContributorsData($city): array
    {
        $results = $this->httpClient->retrieve($this->getContributorUris($city));

        return $this->parseContributorsData($results);
    }

    private function getContributorUris(string $city): array
    {
        $endpoint = $this->rootEndpoint.'/search/users';
        $queryString = '?q=type:user+location:'.$city.'+repos:'
            .urlencode('>').'0&sort=repositories&order=desc';
        $pagesAmount = 2;
        $uris = [];
        for ($page = 1; $page <= $pagesAmount; $page++) {
            $uris[] = $endpoint.$queryString.'&page='.$page.'&per_page=100';
        }

        return $uris;
    }

    private function parseContributorsData(array $results): array
    {
        $contributors = [];
        foreach ($results as $result) {
            foreach ($result->items as $foundContributor) {
                $contributors[] = [
                    'id' => $foundContributor->id,
                    'name' => $foundContributor->login,
                ];
            }
        }

        return $contributors;
    }
}
