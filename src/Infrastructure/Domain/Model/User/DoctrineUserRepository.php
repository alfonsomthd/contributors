<?php

namespace ApiDemo\Infrastructure\Domain\Model\User;

use ApiDemo\Domain\Model\User\User;
use ApiDemo\Domain\Model\User\UserId;
use ApiDemo\Domain\Model\User\UserRepository;
use ApiDemo\Infrastructure\Persistence\Doctrine\DoctrineRepository;

class DoctrineUserRepository extends DoctrineRepository implements UserRepository
{
    public function add(User $user)
    {
        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    public function findById(UserId $id)
    {
        return $this->repository->find($id->id());
    }

    public function findByName($name)
    {
        return $this->repository->findOneBy(['name' => $name]);
    }

    public function findBy(array $criteria)
    {
        return $this->repository->findBy($criteria, ['name' => 'ASC']);
    }

    protected function getEntityName()
    {
        return User::class;
    }
}
