<?php

namespace ApiDemo\Infrastructure\Domain\Model\User;

use ApiDemo\Domain\Model\User\User;
use ApiDemo\Domain\Model\User\UserId;
use ApiDemo\Domain\Model\User\UserRepository;

class InMemoryUserRepository implements UserRepository
{
    public function add(User $user)
    {
    }

    public function findById(UserId $id)
    {
    }

    public function findByName($name)
    {
        return null;
    }

    public function findBy(array $criteria = [])
    {
        return [new User(new UserId(), 'InMemoryUsername', 'InMemoryPassword')];
    }
}
