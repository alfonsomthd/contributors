<?php

namespace ApiDemo\Infrastructure\Domain\Model\Contributor;

use ApiDemo\Domain\Model\Contributor\ContributorSearchCriteria;
use ApiDemo\Domain\Model\Contributor\ContributorGateway;
use ApiDemo\Domain\Model\Contributor\Contributor;
use ApiDemo\Infrastructure\Integration\Platform\PlatformClient;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Psr\SimpleCache\CacheInterface;

class HttpContributorGateway implements ContributorGateway
{
    private $cacheClient;
    private $logger;
    private $platformClientLocator;

    public function __construct(
        CacheInterface $cacheClient,
        LoggerInterface $logger,
        ContainerInterface $platformClientLocator
    ) {
        $this->cacheClient = $cacheClient;
        $this->logger = $logger;
        $this->platformClientLocator = $platformClientLocator;
    }

    public function listBy(ContributorSearchCriteria $criteria): array
    {
        $key = $criteria->platformName().'-'.$criteria->city();

        try {
            if ($this->cacheClient->has($key)) {
                $contributorsData = json_decode($this->cacheClient->get($key), true);
            }
        } catch (\Throwable $error) {
            $this->logger->error($error->getMessage(), ['trace' => $error->getTraceAsString()]);
        }

        $contributorsData = empty($contributorsData) ?
            $this->getContributorsData($criteria->platformName(), $criteria->city()) : $contributorsData;

        try {
            if (!$this->cacheClient->has($key)) {
                $this->cacheClient->set($key, json_encode($contributorsData));
            }
        } catch (\Throwable $error) {
            $this->logger->error($error->getMessage(), ['trace' => $error->getTraceAsString()]);
        }

        $contributorsData = $this->getContributorsRequestedAmount($contributorsData, $criteria->amount());

        $contributors = [];
        foreach ($contributorsData as $contributorData) {
            $contributors[] = new Contributor($contributorData['id'], $criteria->platformName(), $contributorData['name']);
        }

        return $contributors;
    }

    private function getContributorsData(string $platformName, string $city): array
    {
        /** @var PlatformClient $platformClient */
        $platformClient = $this->platformClientLocator->get($platformName);

        return $platformClient->getContributorsData($city);
    }

    private function getContributorsRequestedAmount(array $contributorsData, int $amount)
    {
        return array_slice($contributorsData, 0, $amount);
    }
}
