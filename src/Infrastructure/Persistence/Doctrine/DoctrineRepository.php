<?php

namespace ApiDemo\Infrastructure\Persistence\Doctrine;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

abstract class DoctrineRepository
{
    /** @var EntityManager */
    protected $entityManager;
    /** @var EntityRepository */
    protected $repository;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository($this->getEntityName());
    }

    abstract protected function getEntityName();
}
