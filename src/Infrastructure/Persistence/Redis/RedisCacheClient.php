<?php

namespace ApiDemo\Infrastructure\Persistence\Redis;

use Symfony\Component\Cache\Simple\RedisCache;

class RedisCacheClient extends RedisCache
{
    const TTL = 60;
    
    public function __construct(\Redis $redisClient)
    {
        parent::__construct($redisClient, '', self::TTL);
    }
}
