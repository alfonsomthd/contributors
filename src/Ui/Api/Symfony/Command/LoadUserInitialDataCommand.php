<?php

namespace ApiDemo\Ui\Api\Symfony\Command;

use ApiDemo\Application\Service\User\CreateUserCommand;
use ApiDemo\Domain\Model\User\UserAlreadyExistsException;
use ApiDemo\Infrastructure\Security\Symfony\SecurityUser;
use League\Tactician\CommandBus;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class LoadUserInitialDataCommand extends Command
{
    private $passwordEncoder;
    private $commandBus;

    public function __construct(
        $name = null,
        UserPasswordEncoderInterface $passwordEncoder,
        CommandBus $commandBus
    ) {
        parent::__construct($name);

        $this->passwordEncoder = $passwordEncoder;
        $this->commandBus = $commandBus;
    }

    protected function configure()
    {
        $this->setName('api:load-user-initial-data')
            ->setDescription('Load user fixtures in database.')
            ->setHelp('Load user fixtures in database.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        try {
            $users = ['admin' => 'admin1', 'test' => 'test1'];

            foreach ($users as $username => $password) {
                try {
                    $createUserCommand = new CreateUserCommand();
                    $createUserCommand->name = $username;

                    $securityUser = new SecurityUser($createUserCommand->name);

                    $createUserCommand->password = $this->passwordEncoder->encodePassword($securityUser, $password);

                    $userData = $this->commandBus->handle($createUserCommand);

                    $output->writeln('<info>Created user "'.$userData['name'].'" with id: '.$userData['id'].'</info>');
                } catch (UserAlreadyExistsException $error) {
                    $output->writeln('<comment>'.$error->getMessage().'</comment>');
                }
            }
        } catch (\Throwable $error) {
            $output->writeln('<error>'.$error->getMessage().'</error>');
        }
    }
}
