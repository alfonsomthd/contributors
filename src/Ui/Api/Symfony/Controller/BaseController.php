<?php

namespace ApiDemo\Ui\Api\Symfony\Controller;

use ApiDemo\Domain\Model\User\UserAlreadyExistsException;
use League\Tactician\CommandBus;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class BaseController extends Controller
{
    const SHARED_MAX_AGE = 60;

    protected $logger;
    protected $commandBus;

    public function __construct(LoggerInterface $logger, CommandBus $commandBus)
    {
        $this->logger = $logger;
        $this->commandBus = $commandBus;
    }

    public function welcome()
    {
        $response = new JsonResponse('Welcome!', Response::HTTP_OK);
        $response->setSharedMaxAge(self::SHARED_MAX_AGE);

        return $response;
    }

    /**
     * @param Request $request
     * @return \stdClass
     */
    protected function getRequestData(Request $request)
    {
        return json_decode($request->getContent());
    }

    protected function handleError(\Throwable $error)
    {
        $errorClass = get_class($error);
        $errorMessage = utf8_encode($error->getMessage());

        switch ($errorClass) {
            case BadCredentialsException::class:
                $statusCode = Response::HTTP_UNAUTHORIZED;
                break;
            case UserAlreadyExistsException::class:
                $statusCode = Response::HTTP_CONFLICT;
                break;
            case \InvalidArgumentException::class:
                $statusCode = Response::HTTP_BAD_REQUEST;
                break;
            default:
                $this->logger->error($errorMessage, ['trace' => $error->getTraceAsString()]);

                $errorMessage = 'Internal server error.';
                $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
                break;
        }

        return $this->json($errorMessage, $statusCode);
    }
}
