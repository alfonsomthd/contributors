<?php

namespace ApiDemo\Ui\Api\Symfony\Controller;

use ApiDemo\Application\Service\User\CreateUserCommand;
use ApiDemo\Application\Service\User\GetUsersCommand;
use ApiDemo\Infrastructure\Security\Symfony\SecurityUser;
use Lexik\Bundle\JWTAuthenticationBundle\Encoder\JWTEncoderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;

class UserController extends BaseController
{
    public function login(
        Request $request,
        GetUsersCommand $getUsersCommand,
        UserPasswordEncoderInterface $passwordEncoder,
        JWTEncoderInterface $jwtEncoder
    ) {
        try {
            $requestData = $this->getRequestData($request);
            $requestData->username = $requestData->username ?? null;
            $requestData->password = $requestData->password ?? null;

            $getUsersCommand->name = $requestData->username;

            $userData = $this->commandBus->handle($getUsersCommand);

            if (empty($userData)) {
                throw new BadCredentialsException('Invalid credentials.');
            }

            $securityUser = new SecurityUser($userData['name'], $userData['password']);

            $isValid = $passwordEncoder->isPasswordValid($securityUser, $requestData->password);

            if (!$isValid) {
                throw new BadCredentialsException('Invalid credentials.');
            }

            $token = $jwtEncoder->encode([
                    'username' => $securityUser->GetUsername(),
                    'exp' => time() + 3600
                ]);

            return $this->json(['token' => $token], Response::HTTP_OK);
        } catch (\Throwable $error) {
            return $this->handleError($error);
        }
    }

    public function getAll(GetUsersCommand $getUsersCommand)
    {
        try {
            $users = $this->commandBus->handle($getUsersCommand);

            return $this->json($users, Response::HTTP_OK);
        } catch (\Throwable $error) {
            return $this->handleError($error);
        }
    }

    public function create(
        Request $request,
        CreateUserCommand $createUserCommand,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        try {
            $requestData = $this->getRequestData($request);
            $requestData->username = $requestData->username ?? null;
            $requestData->password = $requestData->password ?? null;

            $createUserCommand->name = $requestData->username;

            $securityUser = new SecurityUser($createUserCommand->name);

            $createUserCommand->password = $passwordEncoder->encodePassword($securityUser, $requestData->password);

            $user = $this->commandBus->handle($createUserCommand);

            return $this->json($user, Response::HTTP_CREATED);
        } catch (\Throwable $error) {
            return $this->handleError($error);
        }
    }
}
