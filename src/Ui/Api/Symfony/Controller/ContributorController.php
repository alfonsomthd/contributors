<?php

namespace ApiDemo\Ui\Api\Symfony\Controller;

use ApiDemo\Application\Service\Contributor\GetContributorsCommand;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ContributorController extends BaseController
{
    public function list(
        Request $request,
        GetContributorsCommand $getContributorsCommand
    ) {
        try {
            $getContributorsCommand->platform = $request->query->get('platform') ?? '';
            $getContributorsCommand->city = $request->query->get('city') ?? null;
            $getContributorsCommand->amount = $request->query->get('amount') ?? null;

            $users = $this->commandBus->handle($getContributorsCommand);

            $response = new JsonResponse($users, Response::HTTP_OK);
            $response->setSharedMaxAge(self::SHARED_MAX_AGE);

            return $response;
        } catch (\Throwable $error) {
            return $this->handleError($error);
        }
    }
}
