<?php

namespace ApiDemo\Application\Service\User;

use ApiDemo\Application\DataTransformer\User\UserDataTransformer;
use ApiDemo\Domain\Model\User\UserRepository;

class GetUsersHandler
{
    private $userRepository;
    private $userDataTransformer;

    public function __construct(
        UserRepository $userRepository,
        UserDataTransformer $userDataTransformer
    ) {
        $this->userRepository = $userRepository;
        $this->userDataTransformer = $userDataTransformer;
    }
    
    public function handle(GetUsersCommand $command)
    {
        $criteria = [];
        if (!empty($command->name)) {
            $criteria['name'] = $command->name;
        }

        $users = $this->userRepository->findBy($criteria);

        if (empty($users)) {
            return [];
        }

        $foundUsers = [];
        foreach ($users as $user) {
            $foundUsers[] = $this->userDataTransformer->transform($user);
        }

        return 1 === count($foundUsers) ? $foundUsers[0] : $foundUsers;
    }
}
