<?php

namespace ApiDemo\Application\Service\User;

use ApiDemo\Application\DataTransformer\User\UserDataTransformer;
use ApiDemo\Domain\Model\User\User;
use ApiDemo\Domain\Model\User\UserAlreadyExistsException;
use ApiDemo\Domain\Model\User\UserId;
use ApiDemo\Domain\Model\User\UserRepository;

class CreateUserHandler
{
    private $userRepository;
    private $userDataTransformer;

    public function __construct(
        UserRepository $userRepository,
        UserDataTransformer $userDataTransformer
    ) {
        $this->userRepository = $userRepository;
        $this->userDataTransformer = $userDataTransformer;
    }
    
    public function handle(CreateUserCommand $command)
    {
        $user = $this->userRepository->findByName($command->name);
        if (!is_null($user)) {
            throw new UserAlreadyExistsException('Username "'.$command->name.'" is already in use.');
        }

        $userId = new UserId();
        $user = new User($userId, $command->name, $command->password);

        $this->userRepository->add($user);

        return $this->userDataTransformer->transform($user);
    }
}
