<?php

namespace ApiDemo\Application\Service\Contributor;

use ApiDemo\Application\DataTransformer\Contributor\ContributorDataTransformer;
use ApiDemo\Domain\Model\Contributor\ContributorSearchCriteria;
use ApiDemo\Domain\Model\Contributor\ContributorGateway;
use ApiDemo\Domain\Model\Platform\Platform;

class GetContributorsHandler
{
    private $contributorGateway;
    private $contributorDataTransformer;

    public function __construct(
        ContributorGateway $contributorGateway,
        ContributorDataTransformer $contributorDataTransformer
    ) {
        $this->contributorGateway = $contributorGateway;
        $this->contributorDataTransformer = $contributorDataTransformer;
    }
    
    public function handle(GetContributorsCommand $command)
    {
        $criteria = new ContributorSearchCriteria(
            new Platform($command->platform),
            $command->city,
            $command->amount
        );
        $foundContributors = $this->contributorGateway->listBy($criteria);

        $contributors = [];
        foreach ($foundContributors as $contributor) {
            $contributors[] = $this->contributorDataTransformer->transform($contributor);
        }

        return $contributors;
    }
}
