<?php

namespace ApiDemo\Application\Service\Contributor;

class GetContributorsCommand
{
    public $platform;
    public $city;
    public $amount;
}
