<?php

namespace ApiDemo\Application\DataTransformer\User;

use ApiDemo\Domain\Model\User\User;

interface UserDataTransformer
{
    public function transform(User $user);
}
