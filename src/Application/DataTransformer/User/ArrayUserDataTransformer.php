<?php

namespace ApiDemo\Application\DataTransformer\User;

use ApiDemo\Domain\Model\User\User;

class ArrayUserDataTransformer implements UserDataTransformer
{
    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => $user->id()->id(),
            'name' => $user->name(),
            'password' => $user->password(),
        ];
    }
}
