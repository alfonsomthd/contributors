<?php

namespace ApiDemo\Application\DataTransformer\Contributor;

use ApiDemo\Domain\Model\Contributor\Contributor;

interface ContributorDataTransformer
{
    public function transform(Contributor $contributor);
}
