<?php

namespace ApiDemo\Application\DataTransformer\Contributor;

use ApiDemo\Domain\Model\Contributor\Contributor;

class ArrayContributorDataTransformer implements ContributorDataTransformer
{
    public function transform(Contributor $contributor)
    {
        return [
            'id' => $contributor->id(),
            'name' => $contributor->name(),
        ];
    }
}
