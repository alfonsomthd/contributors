<?php

namespace ApiDemo\Domain\Model\Contributor;

interface ContributorGateway
{
    public function listBy(ContributorSearchCriteria $criteria): array;
}
