<?php

namespace ApiDemo\Domain\Model\Contributor;

use ApiDemo\Domain\Model\Platform\Platform;

class ContributorSearchCriteria
{
    const CONTRIBUTORS_MIN_AMOUNT = 1;
    const CONTRIBUTORS_MAX_AMOUNT = 150;

    /** @var Platform */
    private $platform;
    /** @var string */
    private $city;
    /** @var int */
    private $amount;

    public function __construct(Platform $platform, $city, $amount)
    {
        $city = strtolower(trim($city));
        if (empty($city)) {
            throw new \InvalidArgumentException('Invalid city: "'.$city.'"');
        }
        $amount = (int) $amount;
        if ($amount < self::CONTRIBUTORS_MIN_AMOUNT) {
            $amount = self::CONTRIBUTORS_MIN_AMOUNT;
        }
        if ($amount > self::CONTRIBUTORS_MAX_AMOUNT) {
            $amount = self::CONTRIBUTORS_MAX_AMOUNT;
        }

        $this->platform = $platform;
        $this->city = $city;
        $this->amount = $amount;
    }

    public function platformName()
    {
        return $this->platform->name();
    }

    public function city()
    {
        return $this->city;
    }

    public function amount()
    {
        return $this->amount;
    }
}
