<?php

namespace ApiDemo\Domain\Model\Contributor;

class Contributor
{
    /** @var int */
    private $id;
    /** @var string */
    private $platform;
    /** @var string */
    private $name;

    public function __construct($id, $platform, $name)
    {
        $id = (int) $id;
        if (empty($id)) {
            throw new \InvalidArgumentException('"id" not found.');
        }
        $platform = (string) $platform;
        if (empty($platform)) {
            throw new \InvalidArgumentException('"platform" not found.');
        }
        $name = (string) $name;
        if (empty($name)) {
            throw new \InvalidArgumentException('"name" not found.');
        }

        $this->id = $id;
        $this->platform = $platform;
        $this->name = $name;
    }

    public function id()
    {
        return $this->id;
    }

    public function platform()
    {
        return $this->platform;
    }

    public function name()
    {
        return $this->name;
    }
}
