<?php

namespace ApiDemo\Domain\Model\Platform;

class Platform
{
    const GITHUB = 'github';
    const AVAILABLE_PLATFORMS = [
        self::GITHUB,
    ];
    /** @var string */
    private $name;

    public function __construct(string $platformName)
    {
        $platformName = strtolower(trim($platformName));
        if (empty($platformName)) {
            $platformName = self::GITHUB;
        } elseif (!in_array($platformName, self::AVAILABLE_PLATFORMS)) {
            throw new \InvalidArgumentException('Invalid platform: "'.$platformName.'"');
        }

        $this->name = $platformName;
    }

    public function name()
    {
        return $this->name;
    }
}
