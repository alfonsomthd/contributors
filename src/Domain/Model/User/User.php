<?php

namespace ApiDemo\Domain\Model\User;

class User
{
    /** @var UserId */
    private $id;
    /** @var string */
    private $name;
    /** @var string */
    private $password;

    public function __construct(UserId $id, $name, $password)
    {
        $name = (string) $name;
        if (empty($name)) {
            throw new \InvalidArgumentException('"name" not found.');
        }

        $password = (string) $password;
        if (empty($password)) {
            throw new \InvalidArgumentException('"password" not found.');
        }

        $this->id = $id;
        $this->name = $name;
        $this->password = $password;
    }

    public function id()
    {
        return $this->id;
    }

    public function name()
    {
        return $this->name;
    }

    public function password()
    {
        return $this->password;
    }
}
