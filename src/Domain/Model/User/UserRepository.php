<?php

namespace ApiDemo\Domain\Model\User;

interface UserRepository
{
    public function add(User $user);
    /**
     * @param UserId $id
     * @return User|null
     */
    public function findById(UserId $id);
    /**
     * @param string $name
     * @return User|null
     */
    public function findByName($name);

    /**
     * @param [] $criteria
     * @return User[]
     */
    public function findBy(array $criteria);
}
